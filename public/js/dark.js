const body = document.body;
let checkbox;

const onMountTheme = () => {
  const theme = localStorage.getItem('theme');
  if (theme != null && theme==='dark') {
    checkbox.checked = true;
  }
}

const toggleTheme = (checked) => {
  if (checked) {
    localStorage.setItem('theme', 'dark');
  } else {
    localStorage.setItem('theme', '');
  }
}

window.addEventListener('DOMContentLoaded', () => {
  checkbox = document.querySelector('#switch');
  onMountTheme();
  checkbox.addEventListener('change', (e) => {
    toggleTheme(e.target.checked);
  });
});
