---
title: "Sobre"
date: 2021-08-21T23:23:14-03:00
draft: false
---

Bem vindo ao meu feudo digital, esse espaço virtual é uma forma com a qual encontrei de documentar alguns projetos e conteúdos que estive trabalhando ou que achei interessante.

Tenho formação superior na área de Engenharia apesar de nunca ter atuado profissionalmente no ramo, possuo experiência na área de desenvolvimento de sistemas e tecnologia da informação e atualmente estou me especializando em alguns tópicos de pesquisa e buscando exploração em vários nichos diferentes.

Sinta-se livre para me contatar por email através do ícone acima, caso tenha interesse para parcerias ou sugestões de assuntos para escrita.

Caso queira colaborar financeiramente com o website, considere realizar um pix para o mesmo e-mail: <td><span>vsaller</span>@yahoo<span>.com</span></td>.
Isso me ajuda a me manter motivado para continuar produzindo conteúdo.

Obrigado pela atenção.
