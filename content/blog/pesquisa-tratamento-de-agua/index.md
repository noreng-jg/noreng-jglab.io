---
title: "Pesquisa: Desafios no tratamento de água"
date: 2024-11-21T23:17:15-03:00
draft: false
tags: ["Química", "Meio-Ambiente", "Sustentabilidade"]
---

# Resumo

O consumo excessivo da realidade moderna abre espaço para o descarte incorreto de resíduos, o que por sua vez acaba por gerar diversos problemas ambientais e poluição dos recursos naturais.

Dentre estes recursos um dos mais afetados é a água. Rios, lagos e oceanos vem sendo comprometidos com a poluição hídrica causada pela não observância da reciclagem e do destino adequado dos materiais residuais. Dois destes tipos de lixo valem a nossa atenção, são eles; plásticos e lixo eletrônico.

A concentração de plásticos em ambientes aquáticos gera ao longo do tempo um aumento de microplásticos nos recursos hídricos, essa concentração vem sendo estudada por diversos pesquisadores e pode estar vinculada a diversos problemas de saúde como a concentração destes micro-resíduos em vários orgãos do corpo-humano como coração e cérebro.

Já o acúmulo de lixo por componentes eletrônicos gera contaminação por metais pesados em ambientes aquáticos, o que é extremamente nocivo ao ser-humano pois esses metais a nível microscópico tende a se acumular ao longo da cadeia alimentar, o que torna o consumo de alimentos de ambientes aquáticos, como peixes, um risco á saúde.

É válido, portanto, o desenvolvimento de sistemas, que permitam de igual forma, o monitoramento e o tratamento destes materiais em ambientes aquáticos de modo a minimizar seu impacto ao ser-humano e também aos ecossistemas que possam vir a ser comprometidos por parte de pŕaticas inadequadas.

Políticas que incentivem a reciclagem, conscientização, criação de aterros sanitários e a separação correta dos lixos com a destinação adequada poderiam ser aplicadas para resolver os problemas de acúmulo de lixo, bem como a busca por soluções que maximizem a reutilização dos materiais descartados na indústria.

______________________________________________________________

# Palavras Chave

*Tratamento de água, reciclagem, microplásticos, metais pesados.*

_______________________________________________________________


# Bibliografia

[1] https://ucsplay.ucs.br/video/limpeza-dos-oceanos-ja-e-realidade/

[2] https://credcarbo.com/carbono/e-possivel-remover-os-microplasticos-dos-oceanos/

[3] https://www.agsolve.com.br/noticias/10407/como-identificar-problemas-na-qualidade-da-agua

[4] https://www.aguaeefluentes.com.br/post/remo%C3%A7%C3%A3o-de-micropl%C3%A1sticos-da-%C3%A1gua

[5] https://www.inovacaotecnologica.com.br/noticias/noticia.php?artigo=filtro-serragem-madeira-captura-microplasticos&id=020125230911

[6] https://www.sciencedirect.com/science/article/pii/S2214714422008960#ks0005

[7] https://jornal.usp.br/ciencias/ciencias-ambientais/poluicao-por-metais-pesados-atinge-vida-marinha-remota-mostra-estudo/

[8] https://microambiental.com.br/analises-de-agua/analise-de-metais-pesados-na-agua/

--------------------
