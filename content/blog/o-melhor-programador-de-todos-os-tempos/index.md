---
title: "O Melhor Programador De Todos Os Tempos"
date: 2024-10-24T20:19:58-03:00
draft: false
tags: ["História", "Biografia"]
---

Terry A. Davis foi um cientista de computadores que trabalhou como programador para algumas empresas norte-americanas ao longo de sua trajetória profissional. 

Mestre em engenharia elétrica, Davis é conhecido por projetar sistemas extremamente complexos de domínio público e por possuir transtornos psicológicos decorrentes de sua esquizofrenia, o que fazia com que apresentasse um complexo messiânico.

# TEMPLE OS

Não satisfeito com os sistemas operacionais disponíveis, Terry decidiu criar um sistema operacional inteiramente do zero, sozinho. Este feito é de proporções tão profundamente colossais que é o equivalente a construção de um arranha-céu por um homem só.

Não apenas Terry projetou todo o sistema operacional sozinho, o que segundo ele viria a ser ao sucessor do segundo templo sagrado de Deus a comando do próprio, feito do qual lhe requeriu uma década de trabalho. Esse mesmo templo apresentava linguagem de programação própria denominada por ele como Holy-C.

![alt-text-2](imgs/temple1.jpeg "title-2")

![alt-text-1](imgs/temple2.jpeg "title-1") ![alt-text-2](imgs/temple3.jpeg "title-2")

*Acima algumas imagens ilustrativas da utilização do TempleOS*


[![title here](https://img.youtube.com/vi/h7gf5M04hdg/0.jpg)](https://www.youtube.com/watch?v=h7gf5M04hdg)

*Acima vídeo explicando o funcionamento do SO em inglês* 

# Outros trabalhos

Terry também produziu um software inteiramente sozinho para plataforma Windows(executado com WINE no vídeo abaixo) chamado **SimStructure** que utilizava para realizar simulações matemáticas de teoria de controle.

Ele é muito semelhante ao MATLAB, só que gratuito, e consegue produzir várias simulações de corpos físicos de diversos tipos. Um possível uso de sua aplicação é a simulação de foguetes com o acréscimo de pesos.

[![title here](https://img.youtube.com/vi/25hLohVZdME/0.jpg)](https://www.youtube.com/watch?v=25hLohVZdME)

*Acima vídeo contendo a explicação e utilzação do SimStructure por Terry* 

# Reflexões a respeito da vida

## Simplicidade

De acordo com Davis: *Um idiota admira complexidade, um gênio admira simplicidade.*

[![title here](https://img.youtube.com/vi/k0qmkQGqpM8/0.jpg)](https://www.youtube.com/watch?v=k0qmkQGqpM8)

# Conclusões

É um pouco triste pensar nos seus últimos anos de sua vida, Terry passou por muitas dificuldades, dentre elas tornou-se um morador de rua, foi encarceirado e morreu sozinho sem ninguém ao seu lado. Apesar de ser um gênio nato, Terry Davis acabou desenvolvendo problemas mentais ao longo de sua vida por conta de seus episódios com esquizofrenia.

Uma coisa a se admirar a seu respeito é a sua devoção a Deus, apesar de algumas visões um tanto não-convencionais de mundo que apresentava, devido ao seu complexo messiânico, possivelmente devido a seus problemas de saúde. Ainda é contestável se sua morte foi um acidente ou suicídio.

A fé de Davis e sua devoção foram a propulsão para que fosse capaz de construir seus trabalhos incríveis. Que possamos aprender com esse homem, que apesar de não ter constituído família, buscou através do seu ofício, edificar a Deus e evangelizar, ainda que de sua maneira, muitos de seus seguidores.


# Bibliografia
[1] [Wikipedia](https://en.wikipedia.org/wiki/Terry_A._Davis)
