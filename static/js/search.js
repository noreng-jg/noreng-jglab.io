let items = null;

document.addEventListener("DOMContentLoaded", (ev) => {
  let elementSearch = document.querySelector('.search-text');

  const startListening = () => {
    elementSearch.addEventListener('keyup', (ev) => {
      searchDomText(elementSearch.value, items);
    })
  }

  startListening();
  items = document.querySelectorAll('.post-content');
})

function searchDomText(text, items) {
  let textElements = Array.from(items).map((v) => v.innerText);
  items.forEach((item) => {
    if (!item.innerText.includes(text)) {
      item.style.display = 'none';
    } else {
      item.style.display = 'block';
    }
  })
}
