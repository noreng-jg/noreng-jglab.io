let menuItem = null;
let onWindow = true;
let windowSize = window.innerWidth; 
let showMenu = windowSize > 860;

// 860 is the minimum width to show lateral menu 
function addScreenListener(menuItem) {
  const mediaQuery = window.matchMedia('(min-width: 860px)')

  //event handler to screen change
  function handleScreenChange(e) {
    if (e.matches) {
      showMenu = true;
    } else {
      showMenu = false;
      menuItem.style.display = 'none';
    }
  }

  menuItem.style.display = showMenu ? 'block' : 'none';

  mediaQuery.addListener(handleScreenChange);
}

document.addEventListener("DOMContentLoaded", (ev) => {
  menuItem = document.querySelector('.menu-table');

  addScreenListener(menuItem);

  document.childNodes[1].addEventListener('mouseleave', (e) => {
    onWindow = false;
    menuItem.style.display = 'none';
  })

  menuItem.addEventListener('mouseleave', (e) => {
    onWindow = false;
    menuItem.style.display = 'none';
  })

  document.childNodes[1].addEventListener('mouseenter', (e) => {
    onWindow = true;
  })
})

function detectHorizontalPosition(x, percentual, operator) {
  switch (operator) {
    case '>':
      return (x / window.innerWidth) > percentual;
    case '<':
      return (x / window.innerWidth) < percentual;
    default:
      return null;
  }
}

document.addEventListener("mousemove", (ev) => {
  if (showMenu && detectHorizontalPosition(ev.x, .85, '>')) {
    menuItem.style.display = 'block';
  } else {
    if (detectHorizontalPosition(ev.x, .6, '<')) {
      menuItem.style.display = 'none';
    }
  }
})
